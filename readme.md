# Mark of the Afflicted

<!-- markdownlint-disable MD013 -->
[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)][License]
[![Open Issues](https://img.shields.io/gitlab/issues/open-raw/widgitlabs/eso/MarkOfTheAfflicted?gitlab_url=https%3A%2F%2Fgitlab.com%2F)][Open Issues]
[![Pipelines](https://gitlab.com/widgitlabs/eso/MarkOfTheAfflicted/badges/main/pipeline.svg)][Pipelines]
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)][Discord]
<!-- markdownlint-restore MD013 -->

Mark of the Afflicted is a fairly simple addon for Elder Scrolls Online
which tracks all of your vampire and werewolf characters and monitors their
bite timers. In addition, it provides a handy map system for locating vampire
altars and werewolf shrines.

Right now, Mark of the Afflicted doesn't do that terribly much, but I want to
take it a lot farther. But, to do that, I need feedback! What can be improved
with the existing setup? What would you like to see added? Let me know, and
I'll see what I can do!

## Support

This is a developer's portal for Mark of the Afflicted and should _not_ be used for support. Please visit the [support page](http://www.esoui.com/portal.php?uid=31772&a=listbugs) if you need to submit a support request.

[License]: https://gitlab.com/widgitlabs/eso/MarkOfTheAfflicted/blob/main/license.txt
[Open Issues]: https://gitlab.com/widgitlabs/eso/MarkOfTheAfflicted/-/boards
[Pipelines]: https://gitlab.com/widgitlabs/eso/MarkOfTheAfflicted/pipelines
[Discord]: https://discord.gg/jrydFBP
